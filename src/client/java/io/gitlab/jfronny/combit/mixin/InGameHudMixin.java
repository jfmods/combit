package io.gitlab.jfronny.combit.mixin;

import io.gitlab.jfronny.combit.*;
import net.minecraft.client.*;
import net.minecraft.client.gui.DrawContext;
import net.minecraft.client.gui.hud.*;
import net.minecraft.client.option.*;
import net.minecraft.client.render.RenderTickCounter;
import org.spongepowered.asm.mixin.*;
import org.spongepowered.asm.mixin.injection.*;
import org.spongepowered.asm.mixin.injection.callback.*;

@Mixin(InGameHud.class)
public class InGameHudMixin {
    @Shadow @Final private MinecraftClient client;
    private AttackIndicator combit$attackIndicator;

    @Inject(at = @At("HEAD"), method = "renderCrosshair(Lnet/minecraft/client/gui/DrawContext;Lnet/minecraft/client/render/RenderTickCounter;)V")
    private void renderCrosshair(DrawContext context, RenderTickCounter tickCounter, CallbackInfo ci) {
        if (CombitConfig.cooldownProgressOverride >= 0) {
            combit$attackIndicator = this.client.options.getAttackIndicator().getValue();
            this.client.options.getAttackIndicator().setValue(AttackIndicator.OFF);
        }
    }

    @Inject(at = @At("RETURN"), method = "renderCrosshair(Lnet/minecraft/client/gui/DrawContext;Lnet/minecraft/client/render/RenderTickCounter;)V")
    private void renderCrosshairPost(DrawContext context, RenderTickCounter tickCounter, CallbackInfo ci) {
        if (CombitConfig.cooldownProgressOverride >= 0) {
            this.client.options.getAttackIndicator().setValue(combit$attackIndicator);
        }
    }
}
