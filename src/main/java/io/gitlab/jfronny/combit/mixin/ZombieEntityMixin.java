package io.gitlab.jfronny.combit.mixin;

import io.gitlab.jfronny.combit.CombitConfig;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.mob.HostileEntity;
import net.minecraft.entity.mob.ZombieEntity;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;

@Mixin(ZombieEntity.class)
public class ZombieEntityMixin extends HostileEntity {
    protected ZombieEntityMixin(EntityType<? extends HostileEntity> entityType, World world) {
        super(entityType, world);
    }

    @Redirect(method = "tickMovement()V", at = @At(value = "INVOKE", target = "Lnet/minecraft/entity/mob/ZombieEntity;isAffectedByDaylight()Z"))
    private boolean redirectAffectedByDaylight(ZombieEntity instance) {
        return CombitConfig.skeletonsBurn && isAffectedByDaylight();
    }
}
