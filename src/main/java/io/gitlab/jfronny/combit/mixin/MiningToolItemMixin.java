package io.gitlab.jfronny.combit.mixin;

import io.gitlab.jfronny.combit.CombitConfig;
import net.minecraft.block.Block;
import net.minecraft.component.ComponentMap;
import net.minecraft.component.DataComponentTypes;
import net.minecraft.component.type.AttributeModifierSlot;
import net.minecraft.component.type.AttributeModifiersComponent;
import net.minecraft.entity.attribute.EntityAttributeModifier;
import net.minecraft.entity.attribute.EntityAttributes;
import net.minecraft.item.*;
import net.minecraft.registry.tag.TagKey;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.ArrayList;
import java.util.List;

@Mixin(MiningToolItem.class)
public abstract class MiningToolItemMixin extends Item {
    public MiningToolItemMixin(Settings settings) {
        super(settings);
    }

    @Inject(at = @At("RETURN"), method = "<init>(Lnet/minecraft/item/ToolMaterial;Lnet/minecraft/registry/tag/TagKey;FFLnet/minecraft/item/Item$Settings;)V")
    private void modifyAttackDamage(ToolMaterial material, TagKey<Block> effectiveBlocks, float attackDamage, float attackSpeed, Settings settings, CallbackInfo ci) {
        double factor = ((MiningToolItem)(Object)this) instanceof AxeItem ? CombitConfig.axeAttackDamageFactor : CombitConfig.weaponAttackDamageFactor;
        if (factor >= 0) {
            ItemSettingsAccessor isa = (ItemSettingsAccessor) settings;
            ComponentMap.Builder cb = isa.combit$getComponents();
            if (cb == null) {
                cb = ComponentMap.builder().addAll(DataComponentTypes.DEFAULT_ITEM_COMPONENTS);
                isa.combit$setComponents(cb);
            }
            ComponentMapBuilderAccessor cbi = (ComponentMapBuilderAccessor) cb;
            List<AttributeModifiersComponent.Entry> entries = new ArrayList<>();
            boolean showInTooltip = false;
            AttributeModifiersComponent amcOrig = (AttributeModifiersComponent) cbi.combit$getComponents().get(DataComponentTypes.ATTRIBUTE_MODIFIERS);
            if (amcOrig != null) {
                entries.addAll(amcOrig.modifiers());
                showInTooltip = amcOrig.showInTooltip();
            }

            AttributeModifiersComponent.Entry damageEntry = null;
            for (AttributeModifiersComponent.Entry entry : entries) {
                if (entry.attribute().equals(EntityAttributes.ATTACK_DAMAGE)) {
                    damageEntry = entry;
                }
            }
            double damage = 5;
            if (damageEntry != null) {
                entries.remove(damageEntry);
                damage = damageEntry.modifier().value();
            }

            damageEntry = new AttributeModifiersComponent.Entry(
                    EntityAttributes.ATTACK_DAMAGE,
                    new EntityAttributeModifier(
                            BASE_ATTACK_DAMAGE_MODIFIER_ID,
                            damage * factor,
                            EntityAttributeModifier.Operation.ADD_VALUE),
                    AttributeModifierSlot.MAINHAND);

            entries.add(damageEntry);
            cb.add(DataComponentTypes.ATTRIBUTE_MODIFIERS, new AttributeModifiersComponent(entries, showInTooltip));
        }
    }
}
