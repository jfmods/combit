package io.gitlab.jfronny.combit.mixin;

import io.gitlab.jfronny.combit.Combit;
import io.gitlab.jfronny.combit.CombitConfig;
import io.gitlab.jfronny.combit.events.EntityHurtEvent;
import io.gitlab.jfronny.combit.events.EntityKnockbackEvent;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.attribute.EntityAttribute;
import net.minecraft.entity.attribute.EntityAttributeInstance;
import net.minecraft.entity.attribute.EntityAttributes;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.registry.entry.RegistryEntry;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.ActionResult;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(LivingEntity.class)
public abstract class LivingEntityMixin {
    @Shadow public abstract float getMaxHealth();
    @Shadow public abstract void setHealth(float health);
    @Shadow @Nullable public abstract EntityAttributeInstance getAttributeInstance(RegistryEntry<EntityAttribute> attribute);

    @Inject(at = @At("TAIL"), method = "applyDamage", cancellable = true)
    private void onEntityHurt(ServerWorld world, DamageSource source, float amount, CallbackInfo ci) {
        ActionResult result = EntityHurtEvent.EVENT.invoker().hurtEntity((LivingEntity) (Object) this, source, amount);
        if (result == ActionResult.FAIL) {
            ci.cancel();
        }
    }

    @Inject(at = @At("HEAD"), method = "takeKnockback", cancellable = true)
    private void onTakingKnockback(double strength, double x, double z, CallbackInfo ci) {
        ActionResult result = EntityKnockbackEvent.EVENT.invoker().takeKnockback((LivingEntity) (Object) this, strength, x, z);
        if (result == ActionResult.FAIL) {
            ci.cancel();
        }
    }

    @Inject(at = @At("RETURN"), method = "<init>(Lnet/minecraft/entity/EntityType;Lnet/minecraft/world/World;)V")
    private void injectMaxHealth(EntityType<?> entityType, World world, CallbackInfo ci) {
        if (!Combit.idMatches(EntityType.getId(entityType).toString(), CombitConfig.entityHealthBlacklist)) {
            EntityAttributeInstance att = getAttributeInstance(EntityAttributes.MAX_HEALTH);
            att.setBaseValue(att.getBaseValue() * CombitConfig.entityHealthFactor);
            setHealth(getMaxHealth());
        }
        else System.out.println("Skipping " + EntityType.getId(entityType).toString());
    }
}
