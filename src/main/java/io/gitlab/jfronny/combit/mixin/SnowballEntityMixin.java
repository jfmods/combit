package io.gitlab.jfronny.combit.mixin;

import io.gitlab.jfronny.combit.CombitConfig;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.thrown.SnowballEntity;
import net.minecraft.entity.projectile.thrown.ThrownItemEntity;
import net.minecraft.util.hit.EntityHitResult;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(SnowballEntity.class)
public abstract class SnowballEntityMixin extends ThrownItemEntity {
    public SnowballEntityMixin(EntityType<? extends ThrownItemEntity> entityType, World world) {
        super(entityType, world);
    }

    @Inject(method = "onEntityHit(Lnet/minecraft/util/hit/EntityHitResult;)V", at = @At("TAIL"))
    private void onEntityHit(EntityHitResult entityHitResult, CallbackInfo ci) {
        Entity e = entityHitResult.getEntity();
        if (!(e instanceof PlayerEntity pe) || !pe.getAbilities().invulnerable) {
            if (CombitConfig.snowballKnockbackFactor != 0) {
                e.setVelocity(e.getVelocity().add(this.getVelocity().normalize().multiply(CombitConfig.snowballKnockbackFactor)));
                e.velocityModified = true;
            }
            if (CombitConfig.snowballDamage > 0) {
                e.serverDamage(getDamageSources().thrown(this, this.getOwner()), CombitConfig.snowballDamage);
            }
        }
    }
}
