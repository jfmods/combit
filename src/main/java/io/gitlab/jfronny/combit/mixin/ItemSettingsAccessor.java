package io.gitlab.jfronny.combit.mixin;

import net.minecraft.component.ComponentMap;
import net.minecraft.item.Item;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

@Mixin(Item.Settings.class)
public interface ItemSettingsAccessor {
    @Accessor("components") ComponentMap.Builder combit$getComponents();
    @Accessor("components") void combit$setComponents(ComponentMap.Builder components);
}
