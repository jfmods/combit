package io.gitlab.jfronny.combit.mixin;

import io.gitlab.jfronny.combit.CombitConfig;
import net.minecraft.entity.mob.MobEntity;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(MobEntity.class)
public class MobEntityMixin {
    @Inject(method = "isAffectedByDaylight()Z", at = @At("RETURN"), cancellable = true)
    private void modifyDaylight(CallbackInfoReturnable<Boolean> cir) {
        if (CombitConfig.alwaysBurn) cir.setReturnValue(true);
    }
}
