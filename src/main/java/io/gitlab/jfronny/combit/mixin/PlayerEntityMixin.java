package io.gitlab.jfronny.combit.mixin;

import io.gitlab.jfronny.combit.CombitConfig;
import io.gitlab.jfronny.combit.events.EntityHurtEvent;
import io.gitlab.jfronny.combit.events.PlayerAttackEvent;
import net.minecraft.entity.Entity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.ActionResult;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(PlayerEntity.class)
public class PlayerEntityMixin {
    @Inject(at = @At("TAIL"), method = "applyDamage(Lnet/minecraft/server/world/ServerWorld;Lnet/minecraft/entity/damage/DamageSource;F)V", cancellable = true)
    private void onEntityHurt(ServerWorld world, DamageSource source, float amount, CallbackInfo info) {
        ActionResult result = EntityHurtEvent.EVENT.invoker().hurtEntity((PlayerEntity) (Object) this, source, amount);
        if (result == ActionResult.FAIL) {
            info.cancel();
        }
    }

    @Inject(at = @At("HEAD"), method = "attack(Lnet/minecraft/entity/Entity;)V", cancellable = true)
    private void onPlayerAttack(final Entity target, CallbackInfo info) {
        ActionResult result = PlayerAttackEvent.EVENT.invoker().attackEntity((PlayerEntity) (Object) this, target);
        if (result == ActionResult.FAIL) {
            info.cancel();
        }
    }

    @Inject(at = @At("HEAD"), method = "getAttackCooldownProgress(F)F", cancellable = true)
    public void getAttackCooldownProgress(float baseTime, CallbackInfoReturnable<Float> info) {
        if (CombitConfig.cooldownProgressOverride >= 0) {
            info.setReturnValue(CombitConfig.cooldownProgressOverride);
        }
    }

    @Inject(at = @At("HEAD"), method = "getAttackCooldownProgressPerTick()F", cancellable = true)
    public void getAttackCooldownProgressPerTick(CallbackInfoReturnable<Float> info) {
        if (CombitConfig.cooldownProgressPerTickOverride >= 0) {
            info.setReturnValue(CombitConfig.cooldownProgressPerTickOverride);
        }
    }
}
