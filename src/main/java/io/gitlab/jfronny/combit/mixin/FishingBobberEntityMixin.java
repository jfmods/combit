package io.gitlab.jfronny.combit.mixin;

import io.gitlab.jfronny.combit.CombitConfig;
import net.minecraft.entity.Entity;
import net.minecraft.entity.projectile.FishingBobberEntity;
import net.minecraft.util.math.Vec3d;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;

@Mixin(FishingBobberEntity.class)
public class FishingBobberEntityMixin {
    @Redirect(method = "pullHookedEntity(Lnet/minecraft/entity/Entity;)V", at = @At(value = "INVOKE", target = "Lnet/minecraft/util/math/Vec3d;multiply(D)Lnet/minecraft/util/math/Vec3d;"))
    private Vec3d modifyMultiply(Vec3d instance, double value) {
        return instance.multiply(value * CombitConfig.fishingBobberPullFactor);
    }

    @Redirect(method = "pullHookedEntity(Lnet/minecraft/entity/Entity;)V", at = @At(value = "INVOKE", target = "Lnet/minecraft/entity/Entity;setVelocity(Lnet/minecraft/util/math/Vec3d;)V"))
    private void setVelocityModified(Entity instance, Vec3d velocity) {
        instance.setVelocity(velocity);
        instance.velocityModified = true;
    }
}
