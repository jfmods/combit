package io.gitlab.jfronny.combit.mixin;

import io.gitlab.jfronny.combit.CombitConfig;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.mob.CreeperEntity;
import net.minecraft.entity.mob.HostileEntity;
import net.minecraft.entity.mob.PathAwareEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(HostileEntity.class)
public class HostileEntityMixin extends PathAwareEntity {
    protected HostileEntityMixin(EntityType<? extends PathAwareEntity> entityType, World world) {
        super(entityType, world);
    }

    @Inject(method = "tickMovement()V", at = @At("HEAD"))
    private void creeperFireTick(CallbackInfo ci) {
        if ((Object)this instanceof CreeperEntity ce) {
            boolean shouldBurn = CombitConfig.creepersBurn && isAffectedByDaylight();
            if (shouldBurn) {
                ItemStack itemStack = getEquippedStack(EquipmentSlot.HEAD);
                if (!itemStack.isEmpty()) {
                    if (itemStack.isDamageable()) {
                        itemStack.setDamage(itemStack.getDamage() + this.random.nextInt(2));
                        if (itemStack.getDamage() >= itemStack.getMaxDamage()) {
                            sendEquipmentBreakStatus(itemStack.getItem(), EquipmentSlot.HEAD);
                            equipStack(EquipmentSlot.HEAD, ItemStack.EMPTY);
                        }
                    }

                    shouldBurn = false;
                }

                if (shouldBurn) {
                    this.setOnFireFor(8);
                }
            }

            if (CombitConfig.creepersExplodeWhenBurning && isOnFire()) {
                ce.ignite();
            }
        }
    }
}
