package io.gitlab.jfronny.combit;

import io.gitlab.jfronny.combit.events.*;
import net.fabricmc.api.ModInitializer;
import net.minecraft.entity.*;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.text.Text;
import net.minecraft.util.ActionResult;

import java.util.Set;

public class Combit implements ModInitializer {
    @Override
    public void onInitialize() {
        EntityHurtEvent.EVENT.register((entity, source, amount) -> {
            if (entity.getEntityWorld().isClient) return ActionResult.PASS;
            if (CombitConfig.debug && entity instanceof PlayerEntity pe) {
                String debugSource;
                Entity trueSource = source.getAttacker();
                if (trueSource == null || EntityType.getId(trueSource.getType()) == null) {
                    debugSource = "null";
                } else {
                    debugSource = EntityType.getId(trueSource.getType()).toString();
                }
                String message = String.format("Type of damage received: %s\nAmount: %.3f\nTrue Source (mob id): %s\n",
                        source.getName(), amount, debugSource);
                pe.sendMessage(Text.literal(message), false);
            }

            if (CombitConfig.excludePlayers && entity instanceof PlayerEntity) return ActionResult.PASS;
            if (CombitConfig.excludeAllMobs && !(entity instanceof PlayerEntity)) return ActionResult.PASS;
            if (idMatches(getId(entity), CombitConfig.targetEntityWhitelist)) return ActionResult.PASS;
            if (idMatches(source.getName(), CombitConfig.damageSourceWhitelist)) return ActionResult.PASS;

            Entity attacker = source.getAttacker();
            if (attacker != null && idMatches(getId(attacker), CombitConfig.attackerWhitelist))
                return ActionResult.PASS;

            if (CombitConfig.iFrameInterval >= 0)
                entity.timeUntilRegen = CombitConfig.iFrameInterval;
            return ActionResult.PASS;
        });

        // Knockback module part 1
        EntityKnockbackEvent.EVENT.register((entity, amp, dx, dz) -> {
            if (entity.getEntityWorld().isClient) {
                return ActionResult.PASS;
            }

            if (entity.handSwinging) {
                entity.handSwinging = false;
                return ActionResult.FAIL;
            }
            return ActionResult.PASS;
        });

        // Attack + knockback cancel module
        PlayerAttackEvent.EVENT.register((player, target) -> {
            if (player.getEntityWorld().isClient)
                return ActionResult.PASS;

            if (CombitConfig.debug) {
                player.sendMessage(Text.literal(
                    target instanceof LivingEntity le
                            ? String.format("LivingEntity attacked: %s (%s/%s)", EntityType.getId(target.getType()), le.getHealth(), le.getMaxHealth())
                            : String.format("Entity attacked: %s", EntityType.getId(target.getType()))
                ), false);
            }

            float str = player.getAttackCooldownProgress(0);
            if (str <= CombitConfig.attackCancelThreshold)
                return ActionResult.FAIL;
            if (str <= CombitConfig.knockbackCancelThreshold && target instanceof LivingEntity le)
                le.handSwinging = true;

            return ActionResult.PASS;
        });
    }

    public static String getId(Entity entity) {
        return EntityType.getId(entity.getType()).toString();
    }
    public static boolean idMatches(String entityId, Set<String> matches) {
        for (String id : matches) {
            int starIndex = id.indexOf('*');
            if (starIndex != -1) {
                if (entityId.contains(id.substring(0, starIndex))) return true;
            } else {
                if (entityId.equals(id)) return true;
                if (("minecraft:" + entityId).equals(id)) return true;
                if (entityId.equals("minecraft:" + id)) return true;
            }
        }
        return false;
    }
}
