package io.gitlab.jfronny.combit.events;

import net.fabricmc.fabric.api.event.Event;
import net.fabricmc.fabric.api.event.EventFactory;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.ActionResult;

public interface EntityKnockbackEvent {
    Event<EntityKnockbackEvent> EVENT = EventFactory.createArrayBacked(EntityKnockbackEvent.class,
            (listeners) -> (entity, amp, dx, dz) -> {
                for (EntityKnockbackEvent event : listeners) {
                    ActionResult result = event.takeKnockback(entity, amp, dx, dz);
                    if (result != ActionResult.PASS) {
                        return result;
                    }
                }

                return ActionResult.PASS;
            });

    ActionResult takeKnockback(LivingEntity entity, double amp, double dx, double dz);
}
