package io.gitlab.jfronny.combit.events;

import net.fabricmc.fabric.api.event.Event;
import net.fabricmc.fabric.api.event.EventFactory;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.util.ActionResult;

public interface EntityHurtEvent {
    Event<EntityHurtEvent> EVENT = EventFactory.createArrayBacked(EntityHurtEvent.class,
            (listeners) -> (entity, source, amount) -> {
                for (EntityHurtEvent event : listeners) {
                    ActionResult result = event.hurtEntity(entity, source, amount);
                    if (result != ActionResult.PASS) {
                        return result;
                    }
                }

                return ActionResult.PASS;
            });

    ActionResult hurtEntity(LivingEntity entity, DamageSource source, float amount);
}
