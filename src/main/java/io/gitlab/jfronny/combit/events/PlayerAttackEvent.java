package io.gitlab.jfronny.combit.events;

import net.fabricmc.fabric.api.event.Event;
import net.fabricmc.fabric.api.event.EventFactory;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.ActionResult;

public interface PlayerAttackEvent {
    Event<PlayerAttackEvent> EVENT = EventFactory.createArrayBacked(PlayerAttackEvent.class,
            (listeners) -> (player, target) -> {
                for (PlayerAttackEvent event : listeners) {
                    ActionResult result = event.attackEntity(player, target);
                    if (result != ActionResult.PASS) {
                        return result;
                    }
                }

                return ActionResult.PASS;
            });

    ActionResult attackEntity(PlayerEntity player, Entity target);
}
