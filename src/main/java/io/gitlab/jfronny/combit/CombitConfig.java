package io.gitlab.jfronny.combit;

import io.gitlab.jfronny.libjf.config.api.v2.*;
import net.fabricmc.loader.api.FabricLoader;

import java.util.HashSet;

@JfConfig
public class CombitConfig {
    // Invulnerability
    @Entry public static int iFrameInterval = -1;
    @Entry(min=-1, max = 1) public static double attackCancelThreshold = -1d;
    @Entry(min=-1, max = 1) public static double knockbackCancelThreshold = -1d;
    @Entry public static HashSet<String> attackerWhitelist = null;
    @Entry public static HashSet<String> damageSourceWhitelist = null;
    @Entry public static HashSet<String> targetEntityWhitelist = null;
    @Entry public static boolean excludeAllMobs = false;
    @Entry public static boolean excludePlayers = false;
    // Entity Modification
    @Entry public static boolean skeletonsBurn = true;
    @Entry public static boolean zombiesBurn = true;
    @Entry public static boolean creepersBurn = false;
    @Entry public static boolean creepersExplodeWhenBurning = false;
    @Entry public static boolean alwaysBurn = false;
    // Health
    @Entry public static double entityHealthFactor = 1.5;
    @Entry public static HashSet<String> entityHealthBlacklist = null;
    // Weapons
    @Entry public static float cooldownProgressOverride = -1f;
    @Entry public static float cooldownProgressPerTickOverride = -1f;
    @Entry public static double weaponAttackDamageFactor = 1d;
    @Entry public static double axeAttackDamageFactor = 1d;
    // Knockback
    @Entry public static double snowballKnockbackFactor = 0d;
    @Entry public static float snowballDamage = 0f;
    @Entry public static double eggKnockbackFactor = 0d;
    @Entry public static float eggDamage = 0f;
    @Entry public static double fishingBobberPullFactor = 1d;
    // Debug
    @Entry public static boolean debug = FabricLoader.getInstance().isDevelopmentEnvironment();

    static {
        validate();
    }

    @Verifier public static void validate() {
        if (attackerWhitelist == null) {
            attackerWhitelist = new HashSet<>();
            attackerWhitelist.add("minecraft:slime");
            attackerWhitelist.add("minecraft:magma_cube");
            attackerWhitelist.add("tconstruct:blueslime");
            attackerWhitelist.add("thaumcraft:thaumslime");
        }
        if (targetEntityWhitelist == null) {
            targetEntityWhitelist = new HashSet<>();
        }
        if (damageSourceWhitelist == null) {
            damageSourceWhitelist = new HashSet<>();
            // net.minecraft.entity.damage.DamageSource
            damageSourceWhitelist.add("inFire");
            damageSourceWhitelist.add("lightningBolt");
            damageSourceWhitelist.add("lava");
            damageSourceWhitelist.add("hotFloor");
            damageSourceWhitelist.add("inWall");
            damageSourceWhitelist.add("cactus");
            damageSourceWhitelist.add("outOfWorld");
            damageSourceWhitelist.add("sweetBerryBush");
            damageSourceWhitelist.add("dryout");
        }
        if (entityHealthBlacklist == null) {
            entityHealthBlacklist = new HashSet<>();
            entityHealthBlacklist.add("minecraft:player");
        }
    }

    @Preset public static void v189() {
        iFrameInterval = -1;
        attackCancelThreshold = 0.1;
        knockbackCancelThreshold = 0.75;
        attackerWhitelist = null;
        damageSourceWhitelist = null;
        targetEntityWhitelist = null;
        excludeAllMobs = false;
        excludePlayers = false;
        skeletonsBurn = true;
        zombiesBurn = true;
        creepersBurn = false;
        creepersExplodeWhenBurning = false;
        alwaysBurn = false;
        entityHealthFactor = 1.0;
        entityHealthBlacklist = null;
        cooldownProgressOverride = 0.8f;
        cooldownProgressPerTickOverride = Float.MIN_VALUE;
        weaponAttackDamageFactor = 1d;
        axeAttackDamageFactor = 0.5;
        snowballKnockbackFactor = 1d;
        snowballDamage = 0f;
        eggKnockbackFactor = 1d;
        eggDamage = 0f;
        fishingBobberPullFactor = 1d;
    }

    @Preset public static void madness() {
        iFrameInterval = 0;
        attackCancelThreshold = 0d;
        knockbackCancelThreshold = 0d;
        attackerWhitelist = new HashSet<>();
        damageSourceWhitelist = new HashSet<>();
        targetEntityWhitelist = new HashSet<>();
        excludeAllMobs = false;
        excludePlayers = false;
        skeletonsBurn = false;
        zombiesBurn = false;
        creepersBurn = true;
        creepersExplodeWhenBurning = true;
        alwaysBurn = true;
        entityHealthFactor = 10d;
        entityHealthBlacklist = null;
        cooldownProgressOverride = 1f;
        cooldownProgressPerTickOverride = Float.MAX_VALUE;
        weaponAttackDamageFactor = 0.5;
        axeAttackDamageFactor = 5d;
        snowballKnockbackFactor = -1d;
        snowballDamage = 0f;
        eggKnockbackFactor = -5d;
        eggDamage = 1f;
        fishingBobberPullFactor = 2d;
    }

    @Preset public static void noInvulnerability() {
        iFrameInterval = 0;
    }

    static {
        JFC_CombitConfig.ensureInitialized();
    }
}
