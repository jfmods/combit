ComBit allows you to disable I-Frames, remove cool-downs from weapons and modify the health of entities as well as the attack damage of weapons.
Every tweak is optional and disabled by default.
If you want 1.8-like combat, load the "Minecraft 1.8.9" preset in the config screen